package com.dazn.prototype.application

import android.app.Application
import com.dazn.prototype.BuildConfig
import dagger.hilt.android.HiltAndroidApp
//import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class DaznApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

}